#include <stdio.h>
#include <stdlib.h>

struct Node
{
    int number;
    struct Node *next;
};

// Function prototypes
// Creates a new node with given number and returns pointer to it
struct Node *createNode(int num)
{
    struct Node *newNode = (struct Node *)malloc(sizeof(struct Node));
    newNode->number = num;
    newNode->next = NULL;
    return newNode;
}

// Prints the linked list starting from head node
void printList(struct Node *head)
{
    struct Node *temp = head;
    while (temp != NULL)
    {
        printf("%d -> ", temp->number);
        temp = temp->next;
    }
    printf("NULL\n");
}

// Appends a new node with given number to the list
void append(struct Node **head, int num)
{
    struct Node *newNode = createNode(num);
    if (*head == NULL)
    {
        *head = newNode;
        return;
    }

    struct Node *temp = *head;
    while (temp->next != NULL)
    {
        temp = temp->next;
    }
    temp->next = newNode;
}

// Prepends a new node with given number to the list
void prepend(struct Node **head, int num)
{
    struct Node *newNode = createNode(num);
    if (*head == NULL)
    {
        *head = newNode;
        return;
    }

    newNode->next = *head;
    *head = newNode;
}

// Deletes the first node with given key from the list
void deleteByKey(struct Node **head, int key)
{
    if (*head == NULL)
    {
        return;
    }

    if ((*head)->number == key)
    {
        struct Node *temp = *head;
        *head = (*head)->next;
        free(temp);
        return;
    }

    struct Node *temp = *head;
    while (temp->next != NULL && temp->next->number != key)
    {
        temp = temp->next;
    }

    if (temp->next == NULL)
    {
        return;
    }

    struct Node *nodeToDelete = temp->next;
    temp->next = temp->next->next;
    free(nodeToDelete);
}

// Deletes the first node with given value from the list
void deleteByValue(struct Node **head, int value)
{
    if (*head == NULL)
    {
        return;
    }

    if ((*head)->number == value)
    {
        struct Node *temp = *head;
        *head = (*head)->next;
        free(temp);
        return;
    }

    struct Node *temp = *head;
    while (temp->next != NULL && temp->next->number != value)
    {
        temp = temp->next;
    }

    if (temp->next == NULL)
    {
        return;
    }

    struct Node *nodeToDelete = temp->next;
    temp->next = temp->next->next;
    free(nodeToDelete);
}

// Inserts a new node with given value after the first node with given key
void insertAfterKey(struct Node **head, int key, int value)
{
    struct Node *newNode = createNode(value);

    if (*head == NULL)
    {
        *head = newNode;
        return;
    }

    struct Node *temp = *head;
    while (temp != NULL && temp->number != key)
    {
        temp = temp->next;
    }

    if (temp == NULL)
    {
        return;
    }

    newNode->next = temp->next;
    temp->next = newNode;
}

// Inserts a new node with given newValue after the first node with given searchValue
void insertAfterValue(struct Node **head, int searchValue, int newValue)
{
    struct Node *newNode = createNode(newValue);

    if (*head == NULL)
    {
        *head = newNode;
        return;
    }

    struct Node *temp = *head;
    while (temp != NULL && temp->number != searchValue)
    {
        temp = temp->next;
    }

    if (temp == NULL)
    {
        return;
    }

    newNode->next = temp->next;
    temp->next = newNode;
}

int main()
{
    struct Node *head = NULL;
    int choice, data;

    while (1)
    {
        printf("Linked Lists\n");
        printf("1. Print List\n");
        printf("2. Append\n");
        printf("3. Prepend\n");
        printf("4. Delete by key\n");
        printf("5. Delete by value\n");
        printf("6. Insert after key\n");
        printf("7. Insert after value\n");
        printf("8. Exit\n");
        printf("Enter your choice: ");
        scanf("%d", &choice);

        switch (choice)
        {
        case 1:
            printList(head);
            break;
        case 2:
            printf("Enter data to append: ");
            scanf("%d", &data);
            append(&head, data);
            break;
        case 3:
            printf("Enter data to prepend: ");
            scanf("%d", &data);
            prepend(&head, data);
            break;
        case 4:
            printf("Enter key to delete: ");
            scanf("%d", &data);
            deleteByKey(&head, data);
            break;
        case 5:
            printf("Enter value to delete: ");
            scanf("%d", &data);
            deleteByValue(&head, data);
            break;
        case 6:
            printf("Enter key and value to insert: ");
            scanf("%d %d", &data, &data);
            insertAfterKey(&head, data, data);
            break;
        case 7:
            printf("Enter search value and new value to insert: ");
            scanf("%d %d", &data, &data);
            insertAfterValue(&head, data, data);
            break;
        case 8:
            return 0;
        default:
            printf("Invalid choice. Please try again.\n");
            break;
        }
    }

    return 0;
}
// Questions:
// Implement the prototypes defined above.
// Reimplement the main method using a switch and complete the pending steps.